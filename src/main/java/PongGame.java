

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Timer;
import entities.Ball;
import entities.Booster;
import entities.Paddle;
import java.util.Random;

public class PongGame extends ApplicationAdapter {
    private SpriteBatch batch;
    private Ball ball;
    private Paddle topPaddle;
    private Paddle bottomPaddle;
    private BitmapFont font;
    private int scoreTop = 0;
    private int scoreBottom = 0;
    private int difficultyLevel = 2;
    private float randomOffset = 0;
    private boolean gameStarted = false;
    private boolean gameOver = false;
    private String winner = "";

    private Booster booster;
    private float boosterSpawnTime = 0;
    private boolean speedBoosterActive = false;
    private boolean widthBoosterActive = false;
    private float speedBoosterTime = 0;
    private float widthBoosterTime = 0;



    @Override
    public void create() {
        batch = new SpriteBatch();
        ball = new Ball();
        topPaddle = new Paddle(100, Gdx.graphics.getHeight() - 30, 150, 20);
        bottomPaddle = new Paddle(100, 10, 150, 20);
        font = new BitmapFont();
        ball.reset();
        randomizeBoosterSpawnTime();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (!gameStarted) {
            renderStartScreen();
        }else if (!gameOver) {
        update(Gdx.graphics.getDeltaTime());
        drawGame();
    } else {
        renderGameOverScreen();
    }
    }


    private void update(float deltaTime) {
        if (!gameStarted) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.E)) {
                difficultyLevel = 1;
                gameStarted = true;
            } else if (Gdx.input.isKeyJustPressed(Input.Keys.H)) {
                difficultyLevel = 2;
                gameStarted = true;
            }
        }
        if (booster != null) {
            booster.update(deltaTime);
            if (booster.isOffScreen(Gdx.graphics.getHeight())) {
                booster.dispose();
                booster = null;
            } else if (booster.overlaps(bottomPaddle.getBounds())) {
                activateBooster();
                booster.dispose();
                booster = null;
            }
        }

        boosterSpawnTime -= deltaTime;
        if (boosterSpawnTime <= 0) {
            spawnBooster();
            randomizeBoosterSpawnTime();
        }

        if (gameStarted) {
            handleInput(deltaTime);
            ball.update(deltaTime);
            checkBallOutOfBounds();
            moveBotPaddle(deltaTime);
            checkPaddleCollisions();
            checkGameEnd();
        }
    }
    private void drawGame() {
        batch.begin();
        ball.draw(batch);
        topPaddle.draw(batch);
        bottomPaddle.draw(batch);
        font.draw(batch, "BOT: " + scoreTop, 10, Gdx.graphics.getHeight() - 20);
        font.draw(batch, "Player: " + scoreBottom, 10, 20);
        if (booster != null) {
            booster.draw(batch);
        }
        batch.end();
    }
    private void renderStartScreen() {
        batch.begin();
        font.draw(batch, "Press 'E' for Easy, 'H' for Hard", Gdx.graphics.getWidth() / 2 - 150, Gdx.graphics.getHeight() / 2 + 30);
        batch.end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.E)) {
            difficultyLevel = 1;
            gameStarted = true;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.H)) {
            difficultyLevel = 2;
            gameStarted = true;
        }
    }

    private void handleInput(float deltaTime) {
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            bottomPaddle.x -= bottomPaddle.getSpeed() * deltaTime;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            bottomPaddle.x += bottomPaddle.getSpeed() * deltaTime;
        }
        bottomPaddle.x = Math.max(0, Math.min(bottomPaddle.x, Gdx.graphics.getWidth() - bottomPaddle.getWidth()));
    }
    private void moveBotPaddle(float deltaTime) {
        if (difficultyLevel == 1) {
            moveBotPaddleEasy(deltaTime);
        } else if (difficultyLevel == 2) {
            moveBotPaddleHard(deltaTime);
        }
    }

    private void moveBotPaddleEasy(float deltaTime) {
        if (ball.x > topPaddle.x + topPaddle.getWidth() / 2) {
            topPaddle.x += 300 * deltaTime;
        } else if (ball.x < topPaddle.x + topPaddle.getWidth() / 2) {
            topPaddle.x -= 300 * deltaTime;
        }
        topPaddle.x = Math.max(0, Math.min(topPaddle.x, Gdx.graphics.getWidth() - topPaddle.getWidth()));
    }
    private void moveBotPaddleHard(float deltaTime) {
        if (System.currentTimeMillis() % 1000 < 50) {
            randomOffset = (float) (Math.random() - 0.5) * topPaddle.getWidth();
        }

        float targetPosition = ball.x + randomOffset;

        if (topPaddle.x + topPaddle.getWidth() / 2 < targetPosition) {
            topPaddle.x += 400 * deltaTime;
        } else if (topPaddle.x + topPaddle.getWidth() / 2 > targetPosition) {
            topPaddle.x -= 400 * deltaTime;
        }
        topPaddle.x = Math.max(0, Math.min(topPaddle.x, Gdx.graphics.getWidth() - topPaddle.getWidth()));
    }

    private void checkBallOutOfBounds() {
        if (ball.y < 0) {
            scoreTop++;
            ball.reset();
            ball.speedY = 200;
            ball.speedX = 0;
            ball.hitCount = 0;
        } else if (ball.y > Gdx.graphics.getHeight()) {
            scoreBottom++;
            ball.reset();
            ball.speedY = -200;
            ball.speedX = 0;
            ball.hitCount = 0;
        }
    }

    private void checkPaddleCollisions() {
        if (ball.overlaps(bottomPaddle) || ball.overlaps(topPaddle)) {
            adjustBallDirection(ball, (ball.overlaps(bottomPaddle) ? bottomPaddle : topPaddle));
        }
    }

    private void adjustBallDirection(Ball ball, Paddle paddle) {
        float paddleWidth = paddle.getWidth();
        float hitPoint = ball.x - paddle.x;


        if (hitPoint < paddleWidth / 2) {
            float ratio = 1 - (hitPoint / (paddleWidth / 2));
            ball.speedX = -ratio * 500; //
        } else if (hitPoint > paddleWidth / 2) {
            float ratio = (hitPoint - paddleWidth / 2) / (paddleWidth / 2);
            ball.speedX = ratio * 500;
        } else {
            ball.speedX = 0;
        }
        ball.speedY = -ball.speedY;
        ball.hitCount++;
        if (ball.hitCount % 3 == 0 && ball.hitCount <= 15) {
            ball.speedX *= 1.3;
            ball.speedY *= 1.3;
        }
    }
    private void checkGameEnd() {
        if (scoreTop >= 5) {
            gameOver = true;
            winner = "BOT Wins!";
        } else if (scoreBottom >= 5) {
            gameOver = true;
            winner = "Player Wins!";
        }
    }

    private void renderGameOverScreen() {
        batch.begin();
        font.draw(batch, winner, Gdx.graphics.getWidth() / 2 - 100, Gdx.graphics.getHeight() / 2);
        font.draw(batch, "Press 'N' for New Game, 'Q' to Quit", Gdx.graphics.getWidth() / 2 - 200, Gdx.graphics.getHeight() / 2 - 60);
        batch.end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.N)) {
            gameStarted = false;
            gameOver = false;
            scoreTop = 0;
            scoreBottom = 0;
            ball.reset();
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.Q)) {
            Gdx.app.exit();
        }
    }
    private void randomizeBoosterSpawnTime() {
        boosterSpawnTime = new Random().nextFloat() * (10 - 5) + 5;
    }
    private void spawnBooster() {
        float x = new Random().nextFloat() * (Gdx.graphics.getWidth() - 30);
        float y = Gdx.graphics.getHeight() - 50;
        String type = Math.random() > 0.5 ? "speed" : "expand";
        booster = new Booster(x, y, type);
    }

    private void activateBooster() {
        if (booster.getType().equals("speed")) {
            if (speedBoosterActive) {
                speedBoosterTime += 3;
            } else {
                final float originalSpeed = bottomPaddle.getSpeed();
                bottomPaddle.setSpeed(originalSpeed * 2);
                speedBoosterActive = true;
                speedBoosterTime = 3;

                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        if (speedBoosterTime <= 0) {
                            bottomPaddle.setSpeed(originalSpeed);
                            speedBoosterActive = false;
                        } else {
                            speedBoosterTime -= 0.016;
                        }
                    }
                }, 0, 1 / 60f);
            }
        } else if (booster.getType().equals("expand")) {
            if (widthBoosterActive) {
                widthBoosterTime += 8;
            } else {
                final int originalWidth = bottomPaddle.getWidth();
                bottomPaddle.setWidth(originalWidth + originalWidth / 2);
                widthBoosterActive = true;
                widthBoosterTime = 8;

                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        if (widthBoosterTime <= 0) {
                            bottomPaddle.setWidth(originalWidth);
                            widthBoosterActive = false;
                        } else {
                            widthBoosterTime -= 0.016;
                        }
                    }
                }, 0, 1 / 60f);
            }
        }
    }

    @Override
    public void dispose() {
        batch.dispose();
        ball.dispose();
        topPaddle.dispose();
        bottomPaddle.dispose();
        font.dispose();
    }

}