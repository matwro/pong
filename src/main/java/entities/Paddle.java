package entities;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Paddle {
    private Texture image;
    public float x, y;
    private int width, height;
    private float speed = 300;

    public Paddle(float x, float y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        try {
            image = new Texture("src/main/resources/assets/paddle.png");
        } catch (Exception e) {
            System.out.println("Error loading texture: " + e.getMessage());
        }
    }

    public void draw(SpriteBatch batch) {
        batch.draw(image, x, y, width, height);
    }

    public void dispose() {
        image.dispose();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getSpeed() {
        return speed;
    }
    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
