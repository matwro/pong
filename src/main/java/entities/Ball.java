package entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;



public class Ball {
    private Texture image;
    public float x, y;
    public float speedX, speedY;

    public int hitCount = 0;

    public Ball() {
        image = new Texture("src/main/resources/assets/ball.png");
        reset();
    }

    public void update(float deltaTime) {
        x += speedX * deltaTime;
        y += speedY * deltaTime;
        if (x < 0 || x + getWidth() > Gdx.graphics.getWidth()) {
            speedX = -speedX;
            x = Math.max(0, Math.min(x, Gdx.graphics.getWidth() - getWidth()));
        }

    }

    public void draw(SpriteBatch batch) {
        batch.draw(image, x, y);
    }

    public void dispose() {
        image.dispose();
    }

    public void reset() {
        this.x = Gdx.graphics.getWidth() / 2 - image.getWidth() / 2;
        this.y = Gdx.graphics.getHeight() / 2 - image.getHeight() / 2;
        this.speedX = 0;
        this.speedY = Math.random() > 0.5 ? 200 : -200;
    }

    public float getWidth() {
        return image.getWidth();
    }

    public float getHeight() {
        return image.getHeight();
    }

    public boolean overlaps(Paddle paddle) {
        return x < paddle.x + paddle.getWidth() && x + getWidth() > paddle.x &&
                y < paddle.y + paddle.getHeight() && y + getHeight() > paddle.y;
    }
}