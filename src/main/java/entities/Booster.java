

package entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Booster {
    private Texture texture;
    public Rectangle bounds;
    private String type;

    public Booster(float x, float y, String type) {
        this.type = type;
        texture = new Texture("src/main/resources/assets/booster_" + type + ".png");
        bounds = new Rectangle(x, y, texture.getWidth(), texture.getHeight());
    }

    public void update(float deltaTime) {
        bounds.y -= 150 * deltaTime;
    }

    public void draw(SpriteBatch batch) {
        batch.draw(texture, bounds.x, bounds.y);
    }

    public void dispose() {
        texture.dispose();
    }

    public boolean overlaps(Rectangle other) {
        return bounds.overlaps(other);
    }

    public String getType() {
        return type;
    }
    public boolean isOffScreen(float screenHeight) {
        return this.bounds.y + this.bounds.height < 0;
    }
}